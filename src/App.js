// import logo from './logo.svg';
// import './App.css';

import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Cars from "./pages/Cars";
import Home from "./pages/Home";
// import Footer from "./components/Footer";
// import Header from "./components/Header";
// import Navbar from "./components/Navbar";
// import Service from "./components/Service";
// import WhyUs from "./components/WhyUs";
// import Faq from "./components/Faq";
// import Content from "./components/Content";
// // import Home from "./pages/Home";
// // import Cars from "./pages/Cars";
// import Testimonial from "./components/Testimonial";


function App() {
  return (
   <Router>
     <Routes>
        <Route exact path='/' element={<Home />} />
        <Route path='/cars' element={<Cars />} /> 
     </Routes>

   </Router>
  
  );
}

export default App;
