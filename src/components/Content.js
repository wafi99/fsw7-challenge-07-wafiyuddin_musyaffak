function Content() {
    return (
        <section id="content">
            <div className="container">
                <div className="row ">
                    <div className="col">
                        <div className="card-content">
                            <h2>Sewa Mobil di (Lokasimu) Sekarang</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                                labore et dolore magna aliqua. </p>

                            <a className="btn btn-success" href='/carimobil'>Sewa Mobil Sekarang</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Content