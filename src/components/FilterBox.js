import React from 'react'

function FilterBox() {

    const btnStyle = {
        display: 'none'
    }

  return (
    <div className="container mt-4">
                <div id="filter-box" style={{marginTop: '-7%'}}>
                    <div className="card mb-4">
                        <div className="card-body">
                            <div className="row">
                                <div className="col-lg-10 col-md-12">
                                    <div className="row">
                                        <div className="col-lg-3 col-md-6">
                                            <div className="form-group" id="form-group1"><label>Tipe Driver</label><select
                                                    id="select-driver" className="form-control">
                                                    <option>Pilih Tipe Driver</option>
                                                    <option value="Dengan Sopir">Dengan Sopir</option>
                                                    <option value="Tanpa Sopir">Tanpa Sopir</option>
                                                </select></div>
                                        </div>
                                        <div className="col-lg-3 col-md-6">
                                            <div className="form-group" id="form-group2"><label>Tanggal</label><input
                                                    id="input-tanggal" type="date" className="form-control" /></div>
                                        </div>
                                        <div className="col-lg-3 col-md-6">
                                            <div className="form-group" id="form-group3"><label>Waktu Jemput/Ambil</label><input
                                                    id="input-jemput" type="time" className="form-control" /></div>
                                        </div>
                                        <div className="col-lg-3 col-md-6">
                                            <div className="form-group" id="form-group4"><label>Jumlah Penumpang</label>
                                                <div className="input-group"><input id="input-penumpang" type="number"
                                                        className="form-control" placeholder="Jumlah penumpang" />
                                                    <div className="input-group-append">
                                                        <div className="input-group-text bg-white">
                                                            <img src="assets/img/user-icon.svg" alt="user-icon"/></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-2 col-md-12">
                                    <div className="form-group" id="form-group5"><label>&nbsp;</label><button id="button-cari"
                                            type="button" className="btn bg-green col-sm-12 " style={{backgroundColor: 'green', color: 'white'}}>Cari Mobil</button></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <button id="load-btn" style={btnStyle}>Load</button>
                <button id="clear-btn" style={btnStyle}>Clear</button>
                <div className="row" id="cars-list"></div>
            </div>
  )
}

export default FilterBox