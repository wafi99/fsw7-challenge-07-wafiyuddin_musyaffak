const Footer = () => {
    return (
        <section id="footer">
        <div className="container">
            <div className="row">
                <div className="col-md-3">
                    <div className="footer-1">
                        <p>Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000</p>
                        <p>binarcarrental@gmail.com</p>
                        <p>081-233-334-808</p>
                    </div>
                </div>

                <div className="col-md-3">
                    <div className="footer-2 menu-footer">
                        <p><strong>Our services </strong></p>
                        <p><strong>Why Us</strong></p>
                        <p><strong>Testimonial</strong></p>
                        <p><strong>FAQ</strong></p>
                    </div>
                </div>

                <div className="col-md-3">
                    <div className="text-md-right">
                        <div className="footer-2">
                            <p>Connect with us</p>
                            <ul className="list-unstyled list-inline"/>
                                <div className="list-inline">
                                    <img src="./assets/img/icon_facebook.png" alt=""/>
                                    <img src="./assets/img/icon_instagram.png" alt=""/>
                                    <img src="./assets/img/icon_twitter.png" alt=""/>
                                    <img src="./assets/img/icon_mail.png" alt=""/>
                                    <img src="./assets/img/icon_twitch.png" alt=""/>
                                </div>
                        </div>

                       
                    </div>
                </div>

                <div className="col-md-3">
                    <div className="text-md-right">
                        <div className="footer-">
                            <p>Copyright Binar 2022</p>
                            <img src="./assets/img/logo.png" alt=""/>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
    )
}

export default Footer