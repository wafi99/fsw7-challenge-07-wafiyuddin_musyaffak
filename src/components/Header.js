import { Link } from 'react-router-dom'
function Header() {
    return (
        <>

            <section className="hero">
                <div className="container-fluid">
                    <div className="row align-items-center">
                        <div className="col-lg-1 col-md-1"></div>
                        <div className="col-lg-5 tagline-hero">
                            <h1>Sewa dan Rental Mobil Terbaik di
                                kawasan (Lokasimu)</h1>
                            <p>Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas terbaik dengan harga
                                terjangkau. Selalu siap melayani kebutuhanmu untuk sewa mobil selama 24 jam.</p>
                            <Link to="/cars" className="btn btn-success" href='/cars'>Sewa Mobil Sekarang</Link>
                        </div>
                        <div className="col-lg-6">
                            <img src="./assets/img/img_car.png" alt="mobil" className="mobil img-fluid " />
                        </div>
                    </div>
                </div>

            </section>


        </>
    )
}

export default Header