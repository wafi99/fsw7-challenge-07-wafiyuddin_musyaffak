const Navbar = () => {
    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-transparent position-fixed w-100">
            <div className="container">
                <a className="navbar-brand" href="/"><img src="/assets/img/logo.png" alt=""
                    className="d-inline-block align-text-top" />
                </a>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNav">
                    <ul className="navbar-nav ms-auto">
                        <li className="d-lg-none">
                            <br />
                            {/* <a style={{ marginLeft: 10 }} className="bcr-sidebar">BCR</a> */}
                            <a className="navbar-nav-toggler" href="/"><img src="/assets/img/silang.svg" alt=""
                                className="navbar-nav-toggler" /></a>


                        </li>
                        <li className="nav-item mx-2">
                            <a className="nav-link active" href="/">Our Services</a>
                        </li>
                        <li className="nav-item mx-2">
                            <a className="nav-link active" href="/">Why Us</a>
                        </li>
                        <li className="nav-item mx-2">
                            <a className="nav-link active" href="/">Testimonial</a>
                        </li>
                        <li className="nav-item mx-2">
                            <a className="nav-link active" href="/">FAQ</a>
                        </li>
                        <li className="nav-item mx-2">
                            <button className="main-button">Register</button>
                        </li>


                    </ul>

                </div>
            </div>
        </nav>
    )
}

export default Navbar
