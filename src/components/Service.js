function Service() {
  return (
    <>
         <section id="service">
        <div className="container">
            <div className="row align-items-center">
                <div className="col-lg-6 col-md-12">
                    <img className="img-service img-fluid" src="/assets/img/img_service.png" alt=""/>
                </div>
                <div className="col-lg-6 col-md-12 service-tagline">
                    <h2>Best Car Rental for any kind of trip in (Lokasimu)!</h2>
                    <p>Sewa mobil di (Lokasimu) bersama Binar Car Rental jaminan harga lebih murah dibandingkan yang
                        lain, kondisi mobil baru, serta kualitas pelayanan terbaik untuk perjalanan wisata, bisnis,
                        wedding, meeting, dll.</p>
                    <p>
                        <img src="/assets/img/point.png" alt=""/><span>Mobil Dengan Supir di Bali 12 Jam</span>
                    </p>
                    <p>
                        <img src="/assets/img/point.png" alt=""/><span>Mobil Lepas Kunci di Bali 24 Jam </span>
                    </p>
                    <p>
                        <img src="/assets/img/point.png" alt=""/><span>Sewa Mobil Jangka Panjang Bulanan </span>
                    </p>
                    <p>
                        <img src="/assets/img/point.png" alt=""/><span>Gratis Antar - Jemput Mobil di Bandara</span>
                    </p>
                    <p>
                        <img src="/assets/img/point.png" alt=""/><span>Layanan Airport Transfer / Drop In Out</span>
                    </p>
                </div>
            </div>
        </div>

    </section>
    </>
  )
}

export default Service