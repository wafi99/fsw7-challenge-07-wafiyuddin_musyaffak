// import { useState } from "react"
// import { useDispatch, useSelector } from "react-redux"
// import { setTitle } from "../reducers/ui-store"
import React from "react"
import Content from "../components/Content"
import Faq from "../components/Faq"
import Footer from "../components/Footer"
import Header from "../components/Header"
import Navbar from "../components/Navbar"
import Service from "../components/Service"
import Testimonial from "../components/Testimonial"
import WhyUs from "../components/WhyUs"
const Home = () => {
    // const dispatch = useDispatch()
    // const currentTitle = useSelector(state => state.ui.title)
    // const [ customTitle, setCustomTitle ] = useState(currentTitle)

    return (
        <>
           <Navbar/>
           <Header/>
           <Service/>
           <WhyUs/>
           <Testimonial/>
           <Content/>
           <Faq/>
           <Footer/>
        </>
    )
}

export default Home